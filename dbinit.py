import sqlite3

conn = sqlite3.connect('kiva.sqlite3')
c = conn.cursor()
c.execute('''DROP TABLE IF EXISTS loans;''')
c.execute('''CREATE TABLE loans
              (
              lender_id  char(50),
              loan_id int,
              borrower_name char(50),
              loan_description blob,
              loan_languages  char(10),
              loan_status  char(50),
              funded_amount int,
              paid_amount int,
              image_id int,
              template_id int,
              borrower_activity char(500),
              borrower_activity_sector char(500),
              loan_use blob,
              borrower_location_country_code char(10),
              borrower_location_country char(50),
              borrower_location_town char(100),
              borrower_location_geo_level char(50),
              borrower_location_geo_pairs char(50),
              borrower_location_geo_type char(50),
              partner_id int,
              posted_date  datetime,
              planned_expiration_date datetime,
              loan_amount int,
              borrower_count int
              )
          ''')
conn.commit()
#c.close()